<?php
class ControllerExtensionFeedBunting extends Controller {
    public function index() {
        // Check we're connected
        if (!$this->config->get('module_bunting_account_id')) {
            exit;
        }

        // Check token matches
        $token = (!empty($this->request->get['token'])) ? $this->request->get['token'] : '';
        if ($this->config->get('module_bunting_feed_token') != $token) {
            exit;
        }

        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('tool/image');
        $this->load->model('localisation/currency');
        $this->load->model('localisation/language');

        // Pagination
        if (!empty($this->request->get['size'])) {
            $limit = (int) $this->request->get['size'];
            if ($limit <= 0) {
                $limit = 1;
            }
        } else {
            $limit = 200;
        }

        if (!empty($this->request->get['page'])) {
            $page = (int) $this->request->get['page'];
            if ($page <= 0) {
                $start = 0;
            } else {
                $start = $page * $limit;
            }
        } else {
            $start = 0;
            $page = 1;
        }

        $products = $this->model_catalog_product->getProducts(['start' => $start, 'limit' => $limit]);
        $currencies = $this->model_localisation_currency->getCurrencies();
        $current_currency_code = $this->session->data['currency'];
        $languages = $this->model_localisation_language->getLanguages();
        $current_language_code = $this->session->data['language'];

        $last_page = 'yes';

        if (count($products) == $limit) {
            $query = $this->db->query("SELECT COUNT(*) AS total FROM ".DB_PREFIX."product WHERE status='1'");
            $total = $query->row['total'];

            $last_page_number = $total / $limit;

            if ($page != $last_page_number) {
                $last_page = 'no';
            }
        }

        $output  = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>';
        $output .= '<!DOCTYPE feed SYSTEM "https://' . $this->config->get('module_bunting_subdomain') . '.' . $this->config->get('module_bunting_region_id') . '.bunting.com/feed-' . $this->config->get('module_bunting_website_monitor_id') . '.dtd">';
        $output .= '<feed last_page="'.$last_page.'">';

        foreach ($products as $product) {
            $categories = $this->model_catalog_product->getCategories($product['product_id']);
            if (!$product['image'] || empty($categories)) {
                continue;
            }
            $category_names = [];
            $path = $this->getPath($categories[0]['category_id']);
            if ($path) {
                foreach (explode('_', $path) as $path_id) {
                    $category = $this->model_catalog_category->getCategory($path_id);

                    if ($category) {
                        $category_names[] = $category['name'];
                    }
                }
            }
            $category_string = implode('>', $category_names);
            $output .= '<product>';
            $output .= '  <upc>' . $product['product_id'] . '</upc>';
            $output .= '  <ns>';
            foreach ($languages as $language) {
                $p = $this->getProduct($product['product_id'], $language['language_id']);
                list($lang_code, $locale) = explode('-', $language['code']);
                $lang_code = strtolower($lang_code);
                $output .= '    <'.$lang_code.'><![CDATA[' . $p['name'] . ']]></'.$lang_code.'>';
            }
            $output .= '  </ns>';
            $output .= '  <ps>';
            $output .= '    <' . strtolower($current_currency_code) . '>' . number_format($this->tax->calculate($product['price'], $product['tax_class_id']), 2) . '</' . strtolower($current_currency_code) . '>';
            foreach ($currencies as $currency) {
                if ($currency['code'] != $current_currency_code) {
                    $output .= '    <' . strtolower($currency['code']) . '>' . number_format($this->currency->convert($product['price'], $current_currency_code, $currency['code']), 2) . '</' . strtolower($currency['code']) . '>';
                }
            }
            $output .= '  </ps>';
            $output .= '  <u><![CDATA[' . $this->url->link('product/product', 'product_id=' . $product['product_id']) . ']]></u>';

            if ($product['image']) {
                $output .= '  <iu><![CDATA[' . $this->model_tool_image->resize($product['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height')) . ']]></iu>';
            }

            $output .= '  <c><![CDATA[' . $category_string . ']]></c>';

            if ($product['manufacturer']) {
                $output .= '  <b><![CDATA[' . html_entity_decode($product['manufacturer'], ENT_QUOTES, 'UTF-8') . ']]></b>';
            }

            $specials = $this->getProductSpecials($product['product_id']);

            if (!empty($specials)) {
                $special_end = null;
                $output .= '<oss>';
                foreach ($specials as $special) {
                    $output .= '<' . strtolower($current_currency_code) . '>' . number_format($product['price'] - $special['price'], 2) . '</' . strtolower($current_currency_code) . '>';

                    if ($currency['code'] != $current_currency_code) {
                        $output .= '    <' . strtolower($currency['code']) . '>' . number_format($this->currency->convert($product['price'] - $special['price'], $current_currency_code, $currency['code']), 2) . '</' . strtolower($currency['code']) . '>';
                    }

                    if ($special['date_end'] != '0000-00-00') {
                        $special_end = $special['date_end'];
                    }
                }
                $output .= '</oss>';

                if (!is_null($special_end)) {
                    $to = new DateTime($special_end);
                    $output .= '  <oe><![CDATA[' . $to->format('U') . ']]></oe>';
                }
            }

            $output .= '  <s>' . ((empty($product['quantity'])) ? 'n' : 'y') . '</s>';

            if ($product['meta_keyword']) {
                $output .= '  <kw><![CDATA[' . html_entity_decode($product['meta_keyword'], ENT_QUOTES, 'UTF-8') . ']]></kw>';
            }

            if ($product['ean']) {
                $output .= '  <ean><![CDATA[' . html_entity_decode($product['ean'], ENT_QUOTES, 'UTF-8') . ']]></ean>';
            }

            if ($product['upc']) {
                $output .= '  <upc><![CDATA[' . html_entity_decode($product['upc'], ENT_QUOTES, 'UTF-8') . ']]></upc>';
            }

            if ($product['isbn']) {
                $output .= '  <isbn><![CDATA[' . html_entity_decode($product['isbn'], ENT_QUOTES, 'UTF-8') . ']]></isbn>';
            }

            $output .= '</product>';
        }

        $output .= '</feed>';

        $this->response->addHeader('Content-Type: application/xml');
        $this->response->setOutput($output);
    }

    /**
     * Copied from controller/extension/feed/google_base
     *
     * @param  int $parent_id
     * @param  string $current_path
     * @return string
     */
    protected function getPath($parent_id, $current_path = '') {
        $category_info = $this->model_catalog_category->getCategory($parent_id);

        if ($category_info) {
            if (!$current_path) {
                $new_path = $category_info['category_id'];
            } else {
                $new_path = $category_info['category_id'] . '_' . $current_path;
            }

            $path = $this->getPath($category_info['parent_id'], $new_path);

            if ($path) {
                return $path;
            } else {
                return $new_path;
            }
        }
    }

    /**
     * Copied from admin/model/catalog/product
     *
     * @param  int $product_id
     * @return array
     */
    public function getProductSpecials($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' ORDER BY priority, price");

        return $query->rows;
    }

    /**
     * Copied from catalog/model/catalog/product
     *
     * The language is taken from config using the current default so need this to get specific translated language
     *
     * @param  int $product_id
     * @param  int $language_id
     * @return array
     */
    public function getProduct($product_id, $language_id) {
        $query = $this->db->query("SELECT DISTINCT *, pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special, (SELECT points FROM " . DB_PREFIX . "product_reward pr WHERE pr.product_id = p.product_id AND pr.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "') AS reward, (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . (int)$language_id . "') AS stock_status, (SELECT wcd.unit FROM " . DB_PREFIX . "weight_class_description wcd WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = '" . (int)$language_id . "') AS weight_class, (SELECT lcd.unit FROM " . DB_PREFIX . "length_class_description lcd WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = '" . (int)$language_id . "') AS length_class, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2 WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews, p.sort_order FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$language_id . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

        if ($query->num_rows) {
            return array(
                'product_id'       => $query->row['product_id'],
                'name'             => $query->row['name'],
                'description'      => $query->row['description'],
                'meta_title'       => $query->row['meta_title'],
                'meta_description' => $query->row['meta_description'],
                'meta_keyword'     => $query->row['meta_keyword'],
                'tag'              => $query->row['tag'],
                'model'            => $query->row['model'],
                'sku'              => $query->row['sku'],
                'upc'              => $query->row['upc'],
                'ean'              => $query->row['ean'],
                'jan'              => $query->row['jan'],
                'isbn'             => $query->row['isbn'],
                'mpn'              => $query->row['mpn'],
                'location'         => $query->row['location'],
                'quantity'         => $query->row['quantity'],
                'stock_status'     => $query->row['stock_status'],
                'image'            => $query->row['image'],
                'manufacturer_id'  => $query->row['manufacturer_id'],
                'manufacturer'     => $query->row['manufacturer'],
                'price'            => ($query->row['discount'] ? $query->row['discount'] : $query->row['price']),
                'special'          => $query->row['special'],
                'reward'           => $query->row['reward'],
                'points'           => $query->row['points'],
                'tax_class_id'     => $query->row['tax_class_id'],
                'date_available'   => $query->row['date_available'],
                'weight'           => $query->row['weight'],
                'weight_class_id'  => $query->row['weight_class_id'],
                'length'           => $query->row['length'],
                'width'            => $query->row['width'],
                'height'           => $query->row['height'],
                'length_class_id'  => $query->row['length_class_id'],
                'subtract'         => $query->row['subtract'],
                'rating'           => round($query->row['rating']),
                'reviews'          => $query->row['reviews'] ? $query->row['reviews'] : 0,
                'minimum'          => $query->row['minimum'],
                'sort_order'       => $query->row['sort_order'],
                'status'           => $query->row['status'],
                'date_added'       => $query->row['date_added'],
                'date_modified'    => $query->row['date_modified'],
                'viewed'           => $query->row['viewed']
            );
        } else {
            return false;
        }
    }
}

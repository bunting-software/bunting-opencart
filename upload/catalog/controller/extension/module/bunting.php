<?php
class ControllerExtensionModuleBunting extends Controller {

    /**
     * Fires after catalog/view/common/header
     *
     * $output contains the header view html, we're injecting JS in the head here before it's outputted
     *
     * @return void
     */
    public function header($route, $data, $output) {
        $this->load->model('localisation/currency');

        if ($this->config->get('module_bunting_account_id')) {
            $js_to_inject = '';

            // Home page
            if (!isset($this->request->get['route'])) {
                $js_to_inject .= '<script type="text/javascript">if (typeof window.$_Bunting=="undefined") window.$_Bunting={d:{}}; $_Bunting.d.hp = "yes";</script>';
            }

            // Category page
            if (!empty($this->request->get['route']) && $this->request->get['route'] == 'product/category') {
                $category_path = $this->escape($this->request->get['path']);
                $category_ids = explode('_', $category_path);
                $category_names = [];
                foreach($category_ids as $category_id) {
                    $category = $this->model_catalog_category->getCategory($category_id);
                    $category_names[] = $category['name'];
                }
                $category_string = implode('>', $category_names);
                $js_to_inject .= '<script type="text/javascript">
                    if (typeof window.$_Bunting=="undefined") window.$_Bunting={d:{}};
                    $_Bunting.d.c = "' . $category_string . '"; 
                </script>';
            }

            // Product page
            if (!empty($this->request->get['route']) && $this->request->get['route'] == 'product/product') {
                $this->load->model('catalog/product');
                $this->load->model('tool/image');

                $product = $this->model_catalog_product->getProduct($this->request->get['product_id']);
                $js_to_inject .= '<script type="text/javascript">
                    if (typeof window.$_Bunting=="undefined") window.$_Bunting={d:{}}; // Do not edit
                    $_Bunting.d.vp_upc = ' . $this->escape($this->request->get['product_id']) . ';
                    $_Bunting.d.vp_ns_' . $this->session->data['language'] . ' = "' . $product['name'] . '";
                    $_Bunting.d.vp_ps_' . $this->session->data['currency'] . '  = "' . number_format($product['price'], 2) . '";
                    $_Bunting.d.vp_iu = "' . $this->model_tool_image->resize($product['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height')) . '";
                </script>';
            }

            // Cart page
            if (!empty($this->request->get['route']) && $this->request->get['route'] == 'checkout/cart') {
                $js_to_inject .= '<script type="text/javascript">
                    if (typeof $_Bunting=="undefined") var $_Bunting={d:{}}; // Do not edit
                    $_Bunting.d.cp = new Array(); // Do not edit
                    var bunting_shipping = '.((isset($this->session->data['shipping_method']['cost'])) ? $this->session->data['shipping_method']['cost'] : 0).';
                    bunting_shipping = bunting_shipping.toFixed(2);
                    $_Bunting.d.cdc = bunting_shipping.toString();';

                foreach ($this->cart->getProducts() as $product) {
                    $js_to_inject .= 'var bunting_price = '.$this->escape($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))).';
                            bunting_price = checkDecimal(bunting_price);
                            $_Bunting.d.cp.push([
                                "'.$product['product_id'].'",
                                bunting_price,
                                "'.$product['quantity'].'"
                            ]);';
                }

                $js_to_inject .= 'function checkDecimal(this_price) {
                        this_price = this_price.toFixed(2).toString();
                        var decimal_mark = this_price.charAt(this_price.length-3);
                        if (decimal_mark == ",") {
                            this_price = this_price.replace(",", "*");
                            this_price = this_price.replace(".", "");
                            this_price = this_price.replace("*", ".");
                        }
                        return this_price;
                    }
                </script>';
            }

            if (!empty($this->request->get['route']) && strpos($this->request->get['route'], 'checkout/') !== false) {
                $js_to_inject .= '<script type="text/javascript">if (typeof window.$_Bunting=="undefined") window.$_Bunting={d:{}}; $_Bunting.d.co = "yes";</script>';
            }

            if (!empty($this->request->get['route']) && $this->request->get['route'] == 'checkout/success' && isset($this->session->data['bunting_order_id'])) {
                $this->load->model('checkout/order');
                $order_totals = $this->model_checkout_order->getOrderTotals($this->session->data['bunting_order_id']);
                $products = $this->model_checkout_order->getOrderProducts($this->session->data['bunting_order_id']);

                $shipping = 0;
                foreach ($order_totals as $total) {
                    if ($total['code'] == 'shipping') {
                        $shipping = $total['value'];
                    }
                }

                $js_to_inject .= '<script type="text/javascript">
                    if (typeof $_Bunting=="undefined") var $_Bunting={d:{}}; // Do not edit
                    $_Bunting.vc = ""; // Do not edit
                    $_Bunting.d.uc = "'.$this->escape($this->config->get('module_bunting_unique_code')).'"; // Do not edit
                    $_Bunting.d.op = new Array(); // Do not edit

                    // Edit from here onwards...

                    $_Bunting.d.uoc = "'.$this->escape($this->session->data['bunting_order_id']).'";  // Unique order ID generated by your system (text)

                    var bunting_shipping = '.$shipping.';
                    bunting_shipping = bunting_shipping.toFixed(2);

                    $_Bunting.d.odc = bunting_shipping.toString(); // Delivery cost of the order (number)
                    ';

                foreach ($products as $product) {
                    $js_to_inject .= 'var bunting_price = '.$product['price'].';
                        bunting_price = checkDecimal(bunting_price);
                        $_Bunting.d.op.push([
                            "'.$product['product_id'].'",
                            bunting_price.toString(),
                            "'.$product['quantity'].'"
                        ]);';
                }

                $js_to_inject .= 'function checkDecimal(this_price) {
                        this_price = this_price.toFixed(2).toString();
                        var decimal_mark = this_price.charAt(this_price.length-3);
                        if (decimal_mark == ",") {
                            this_price = this_price.replace(",", "*");
                            this_price = this_price.replace(".", "");
                            this_price = this_price.replace("*", ".");
                        }
                        return this_price;
                    }
                </script>';

                unset($this->session->data['bunting_order_id']);
            }

            if ($this->customer->isLogged()) {
                $js_to_inject .= '<script type="text/javascript">
                    if (typeof window.$_Bunting=="undefined") window.$_Bunting={d:{}}; // Do not edit

                    // 1) Unique account number / code when a visitor is logged in to their account on your site
                    $_Bunting.d.uac = "'.$this->escape($this->customer->getId()).'";

                    // 2) Known visitor information
                    $_Bunting.d.fn = "'.$this->escape($this->customer->getFirstName()).'"; // The visitors forename, if known
                    $_Bunting.d.sn = "'.$this->escape($this->customer->getLastName()).'"; // The visitors surname, if known
                    $_Bunting.d.ea = "'.$this->escape($this->customer->getEmail()).'"; // The visitors email address, if known
                    $_Bunting.d.g = "";  // The visitors gender. Enter "male" or "female" if known, or leave empty
                </script>';
            }

            // Bunting asynchronous tracking code
            $js_to_inject .= '<script type="text/javascript">(function(){if(typeof window.$_Bunting=="undefined")window.$_Bunting={d:{}};$_Bunting.src=("https:"==document.location.protocol?"https://":"http://")+"'.$this->escape($this->config->get('module_bunting_subdomain')).'.bunting.com/call.js?wmID='.$this->escape($this->config->get('module_bunting_website_monitor_id')).'";$_Bunting.s=document.createElement("script");$_Bunting.s.type="text/javascript";$_Bunting.s.async=true;$_Bunting.s.defer=true;$_Bunting.s.charset="UTF-8";$_Bunting.s.src=$_Bunting.src;document.getElementsByTagName("head")[0].appendChild($_Bunting.s)})()</script>';

            return str_replace('<head>', '<head>'.$js_to_inject, $output);
        }
    }

    /**
     * Fires before catalog/controller/checkout/success
     *
     * The order is cleared from the session on this page so we need to grab the order id and save it
     * so that we can use it in the header
     *
     * @return void
     */
    public function success() {
        if (isset($this->session->data['order_id'])) {
            $this->session->data['bunting_order_id'] = $this->session->data['order_id'];
        }
    }

    /**
     * Escape a string
     *
     * @param  string $str
     * @return string
     */
    private function escape($str) {
        return htmlspecialchars($str, ENT_QUOTES, "UTF-8");
    }
}
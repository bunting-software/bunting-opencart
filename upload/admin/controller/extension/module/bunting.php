<?php
class ControllerExtensionModuleBunting extends Controller {

    private $error = array();

    public function index() {

        echo $this->config->get('module_bunting_feed_token');

        $this->load->language('extension/module/bunting');

        $this->load->model('setting/setting');
        $this->load->model('design/layout');
        $this->load->model('user/user');
        $this->load->model('localisation/country');
        $this->load->model('localisation/currency');
        $this->load->model('localisation/language');

        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->addStyle('view/stylesheet/bunting.css');
        $this->document->addScript('view/javascript/bunting/admin.js');
        $this->document->addScript('view/javascript/bunting/validate.js');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && !empty($this->request->post['checksubdomain'])) {
            return $this->existsAction($this->request->post['checksubdomain']);
        }

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            if (!empty($this->request->post['verify_email_address'])) {
                return $this->buntingVerify();
            } elseif (!empty($this->request->post['register_email_address'])) {
                return $this->buntingRegister();
            }

            $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
        } elseif ($this->request->server['REQUEST_METHOD'] == 'GET' && !empty($this->request->get['unlink'])) {
            $this->unlinkBunting();

            $this->session->data['success'] = $this->language->get('text_unlink');

            $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = $this->breadcrumbs();

        $data['action'] = $this->url->link('extension/module/bunting', 'user_token=' . $this->session->data['user_token'], true);

        $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

        $data['user_token'] = $this->session->data['user_token'];

        $data['user_info'] = $this->model_user_user->getUser($this->user->getId());
        $data['shop_name'] = $this->config->get('config_name');
        $data['potential_subdomain'] = strtolower(preg_replace("/[^A-Za-z0-9]/", '', $this->config->get('config_name')));
        $data['phone'] = $this->config->get('config_telephone');

        $data['account_id'] = $this->config->get('module_bunting_account_id');
        $data['bunting_timestamp'] = time();
        $data['bunting_subdomain'] = $this->config->get('module_bunting_subdomain');
        $data['bunting_region_id'] = $this->config->get('module_bunting_region_id');
        $data['bunting_email_address'] = $this->config->get('module_bunting_email');
        $data['bunting_password_api'] = $this->config->get('module_bunting_password_api');
        $account_key = $data['bunting_subdomain'].$data['bunting_email_address'].$data['bunting_timestamp'];
        $data['bunting_hash'] = hash_hmac('sha256', $account_key, 'k2c)D3-@s=Ds23k');

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/bunting', $data));
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'extension/module/bunting')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function install() {
        $this->load->model('setting/event');

        $this->model_setting_event->addEvent('bunting_common_header_after', 'catalog/controller/common/header/after', 'extension/module/bunting/header');
        $this->model_setting_event->addEvent('bunting_checkout_success_before', 'catalog/controller/checkout/success/before', 'extension/module/bunting/success');
    }

    public function uninstall() {
        $this->load->model('setting/setting');
        $this->load->model('setting/event');

        $this->model_setting_setting->deleteSetting('module_bunting');

        $this->model_setting_event->deleteEvent('bunting_common_header_after');
        $this->model_setting_event->deleteEvent('bunting_checkout_success_before');
    }

    protected function breadcrumbs() {
        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $breadcrumbs[] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
        );

        $breadcrumbs[] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/bunting', 'user_token=' . $this->session->data['user_token'], true)
        );

        return $breadcrumbs;
    }

    private function buntingVerify()
    {
        $bunting_data = $this->submitToBunting('verify', [
            'email_address' => $this->request->post['verify_email_address'],
            'password' => $this->request->post['verify_password'],
            'subdomain' => $this->request->post['verify_bunting_subdomain']
        ]);

        $this->buntingResponse($bunting_data);
    }

    private function buntingRegister()
    {
        $locale = $this->model_localisation_country->getCountry($this->config->get('config_country_id'));
        $timezone = DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, $locale['iso_code_2']);

        $address = $this->config->get('config_address');
        $address = $this->processAddress($address);

        $submit_data = [
            'billing' => 'automatic',
            'email_address' => $this->request->post['register_email_address'],
            'password' => $this->request->post['register_password'],
            'confirm_password' => $this->request->post['password_confirmation'],
            'subdomain' => $this->request->post['register_bunting_subdomain'],
            'name' => $this->request->post['company_name'],
            'forename' => $this->request->post['forename'],
            'surname' => $this->request->post['surname'],
            'telephone_number' => $this->request->post['telephone_number'],
            'promotional_code' => $this->request->post['promotional_code'],
            'timezone' => $timezone[0],
            'country' => $locale['iso_code_2'],
            'agency' => 'no'
        ];

        $submit_data = $submit_data+$address;

        $bunting_data = $this->submitToBunting('register', $submit_data);

        return $this->buntingResponse($bunting_data);
    }

    private function processAddress($orig_address)
    {
        $comma_address = preg_replace('#[\n]#', ',', trim($orig_address));
        $address_array = explode(',', $comma_address);
        $address = [];
        if (!empty($address_array)) {
            $address['address_line_1'] = array_shift($address_array);
            $address['postcode'] = array_pop($address_array);
            $i = 2;
            foreach ($address_array as $address_line) {
                $address['address_line_'.$i] = $address_line;
                if ($i < 5) {
                    $i++;
                } else {
                    break;
                }
            }
        } else {
            $address['address_line_1'] = $comma_address;
        }
        ksort($address);
        return $address;
    }

    private function submitToBunting($action, $params)
    {
        $languages = [];
        $currencies = [];

        foreach ($this->model_localisation_currency->getCurrencies() as $currency) {
            $currencies[] = [
                'currency' => $currency['code'],
                'symbol' => (!empty($currency['symbol_left']) ? $currency['symbol_left'] : $currency['symbol_right'])
            ];
        }

        foreach ($this->model_localisation_language->getLanguages() as $language) {
            list($lang_code, $locale) = explode('-', $language['code']);
            $languages[] = strtoupper($lang_code);
        }

        $domain = parse_url(HTTP_CATALOG);
        $timestamp = time();

        $feed_token = md5(sha1(uniqid(mt_rand(), 1)).$timestamp);

        $default_params = [
            'timestamp' => $timestamp,
            'hash' => hash_hmac('sha256', $timestamp, 'k2c)D3-@s=Ds23k'),
            'plugin' => 'opencart',
            'domain_name' => $domain['host'],
            'create_website_monitor' => 'yes',
            'website_name' => $this->config->get('config_name'),
            'languages' => $languages,
            'currencies' => $currencies,
            'website_platform' => 'OpenCart',
            'ecommerce' => 'yes',
            'cart_url' => str_replace('admin/', '', $this->url->link('checkout/cart')),
            'product_feed-url_protocol' => $this->config->get('config_secure') ? 'https://' : 'http://',
            'product_feed-url' => $domain['host'].'/index.php?route=extension/feed/bunting&token='.$feed_token
        ];
        $params = $params+$default_params;
        $params = $this->httpBuildQueryForCurl($params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.bunting.com/plugins/'.$action);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        $data = json_decode($response, true);

        if ($data['success']) {
            $data['email_address'] = $params['email_address'];
            $this->installBunting($data, $feed_token);
        }
        return $data;
    }

    /**
     * Checks whether a remove subdomain exists within Bunting
     * This is a bit of a hack and once Bunting supports a better mechanism, we should use that instead
     *
     * @param string $subdomain
     */
    private function existsAction($subdomain) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://' . $subdomain . '.1.bunting.com/login?a=lost_password');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        $response = curl_exec($ch);

        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200) {
            echo 1;
            return;
        }

        echo 0;
    }

    /**
     * Takes a response from the Bunting API and converts it to a valid frontend module response
     *
     * @param array $buntingData
     * @return void
     */
    private function buntingResponse($buntingData) {
        $response = [];

        if ($buntingData['success']) {
            $response['message'] = $_SESSION['message'] = 'You can now login to Bunting.';
            echo json_encode($response);
            die();
        }

        $response['message'] = 'Please review the errors and try again.';

        if (!isset($buntingData['errors']) || !count($buntingData['errors'])) {
            $buntingData['errors'] = [];
            $response['message'] .= '<br><br>There was a problem connecting your shop to Bunting, please contact Bunting support.';
        }

        if (isset($buntingData['errors']['validation'])) {
            $response['message'] .= '<br><br>' . $buntingData['errors']['validation'];
        }

        $response['errors'] = $buntingData['errors'];
        echo json_encode($response);
        die();
    }

    private function httpBuildQueryForCurl($arrays, &$new = array(), $prefix = null)
    {
        if (is_object($arrays)) {
            $arrays = get_object_vars($arrays);
        }

        foreach ($arrays as $key => $value) {
            $k = isset($prefix) ? $prefix.'['.$key.']':$key;
            if (is_array($value) || is_object($value)) {
                $this->httpBuildQueryForCurl($value, $new, $k);
            } else {
                $new[$k] = $value;
            }
        }
        return $new;
    }

    private function installBunting($data, $feed_token)
    {
        $this->model_setting_setting->editSetting('module_bunting', [
            'module_bunting_status' => 1,
            'module_bunting_account_id' => $data['account_id'],
            'module_bunting_email' => $data['email_address'],
            'module_bunting_website_monitor_id' => $data['website_monitor_id'],
            'module_bunting_unique_code' => $data['unique_code'],
            'module_bunting_subdomain' => $data['subdomain'],
            'module_bunting_region_id' => $data['server_region_subdomain_id'],
            'module_bunting_feed_token' => $feed_token,
            'module_bunting_password_api' => $data['password_api'],
        ]);
    }

    private function unlinkBunting()
    {
        $this->model_setting_setting->deleteSetting('module_bunting');

        return true;
    }
}

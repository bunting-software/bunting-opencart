<?php
// Heading
$_['heading_title'] = 'Bunting Personalization';

// Text
$_['text_extension'] = 'Extensions';
$_['text_success'] = 'Success: You have modified module Bunting!';
$_['text_login'] = 'You can now login to Bunting!';
$_['text_unlink'] = 'Bunting scripts removed successfully';

//Error
$_['error_permission'] = 'Warning: You do not have permission to modify module Bunting!';
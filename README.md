# Installing Bunting on Opencart

To download the plugin click on Downloads in the left hand menu then click the `bunting-softwarex` link.

Next to install the `bunting-opencart` plugin, go the admin dashboard of your site and within the menu on the left, click `Extensions` then
`Installer`. Here please upload the `bunting-opencart.ocmod.zip` if everything works correctly it should display `Success: You have modified extensions!`.
             
             
>Please note that this file must be a `ocmod.zip` file. 


Next in the menu on the left again click `Extensions`. This will navigate you to a extension listing page on the extension 
type dropdown select `Modules` and within this list you should see `Bunting Personalization`. Click the `Install` button on
the right hand side.

Finally click the `Edit` button and sign in to your Bunting account to complete the installation process: Enter your existing bunting account details or
if you are new to Bunting and want to get started, click the `CREATE ACCOUNT` button.

>Note after completing an action within the plugin you will be redirected back to the extensions page, navigate back to the plugin
and you will see that your action has been completed.